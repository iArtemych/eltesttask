//
//  ViewController.swift
//  ELTask
//
//  Created by Artem Chursin on 18.02.2020.
//  Copyright © 2020 Artem Chursin. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    //MARK: - Constants
    let searchController = UISearchController(searchResultsController: nil)
    
    //MARK: - Variables
    var searchResult: GIFData?
    var searchResultCount = 0
    var gifsArray: [GIFObject] = []
    var searchText: String = ""
    
    var isSearchBarEmpty: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }
    //MARK: - Outlets
    @IBOutlet weak var dataTable: UITableView!
    
    //MARK: - LifeStyle ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSearchController()
        
        self.title = "Super GIFs"
        dataTable.delegate = self
        dataTable.dataSource = self
        
        requestConfigurator(searchWord: searchText)
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let fullScreenVC = segue.destination as? FullScreenGIFViewController
        
        if segue.identifier == "toFullScreenGIF" {
            
            if let selectedRow = dataTable.indexPathForSelectedRow?.row{
                fullScreenVC?.imageURL = gifsArray[selectedRow]
            }
        }
    }
    
    //MARK: - Methods
    func requestConfigurator(searchWord: String?) {
        
        let baseURL = "api.giphy.com"
        let searchPath = "/v1/gifs/search"
        let apiKey = "hzUYA9rBIafZO4R0Uial4aLG8voZr8lh"
        let gifsCountToDownload = "10"
        
        var components = URLComponents()
        components.scheme = "https"
        components.host = baseURL
        components.path = searchPath
        components.queryItems = [
            URLQueryItem(name: "api_key", value: apiKey),
            URLQueryItem(name: "q", value: searchWord),
            URLQueryItem(name: "limit", value: gifsCountToDownload)
        ]
        
        guard let url = components.string else {
            return
        }
        searchRequest(url: url)
    }
    //MARK: - Private methods
    private func setupSearchController() {
        
        
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = getRandomSearchText()
        navigationItem.searchController = searchController
        definesPresentationContext = true
        searchController.searchBar.delegate = self
    }
    
    func getRandomSearchText() -> String {
        let baseStrings = ["Cars", "Cats", "Frozen"]
        let randomNumber = Int(arc4random_uniform(2))
        let searchString = baseStrings[randomNumber]
        searchText = searchString
        return searchString
    }
    
    private func searchRequest(url: String) {
        
        NetworkManager.fetchData(url: url) { (result) in
            
            DispatchQueue.main.async {
                
                self.searchResult = result
                self.searchResultCount = result.data.count

                let results = result.data
                self.gifsArray = results
                self.dataTable.reloadData()
                
            }
        }
    }
}
