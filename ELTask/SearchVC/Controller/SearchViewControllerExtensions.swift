//
//  SearchViewControllerExtensions.swift
//  ELTask
//
//  Created by Artem Chursin on 19.02.2020.
//  Copyright © 2020 Artem Chursin. All rights reserved.
//

import Foundation
import UIKit


//MARK: - TableView
extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return searchResultCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "dataCell", for: indexPath) as! DataTableViewCell
        
        cell.activityIndicator.startAnimating()
        cell.gifImage.image = nil
        cell.setupCell(gif: gifsArray[indexPath.row])
        
        return cell
    }
}

extension SearchViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 340
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "toFullScreenGIF", sender: nil)
    }
}

//MARK: - SearchBar
extension SearchViewController: UISearchBarDelegate {
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        

        let searchBar = searchController.searchBar
        guard var text = searchBar.text else {
            return
        }
        if isSearchBarEmpty == true {
            text = getRandomSearchText()
            searchController.searchBar.placeholder = text
        }
        requestConfigurator(searchWord: text)
    }
}
