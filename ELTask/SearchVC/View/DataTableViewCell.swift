//
//  DataTableViewCell.swift
//  ELTask
//
//  Created by Artem Chursin on 19.02.2020.
//  Copyright © 2020 Artem Chursin. All rights reserved.
//

import UIKit

class DataTableViewCell: UITableViewCell {
    
    @IBOutlet weak var gifImage: UIImageView!
    @IBOutlet weak var gifName: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    func setupCell(gif: GIFObject){
 
        activityIndicator.hidesWhenStopped = true

        guard let title = gif.title, let imageURL = gif.images?.fixedHeight?.url else {
            
            return
        }
        
        
        let queue = DispatchQueue.global(qos: .userInteractive)
        queue.async
            {
                let image = UIImage.gif(url: imageURL)
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    self.gifImage.image = image
                    self.gifName.text = title
                }
        }
    }
}
