//
//  ImageObject.swift
//  ELTask
//
//  Created by Artem Chursin on 18.02.2020.
//  Copyright © 2020 Artem Chursin. All rights reserved.
//

import Foundation

struct ImageObject: Decodable {
    
    let fixedHeight: FixedHeight?
    let fixedHeightStill: FixedHeightStill?
    let fixedHeightDownsampled: FixedHeightDownsampled?
    let fixedWidth: FixedWidth?
    let fixedWidthStill: FixedWidthStill?
    let fixedWidthDownsampled: FixedWidthDownsampled?
    let fixedHeightSmall: FixedHeightSmall?
    let fixedHeightSmallStill: FixedHeightSmallStill?
    let fixedWidthSmall: FixedWidthSmall?
    let fixedWidthSmallStill: FixedWidthSmallStill?
    let downsized: Downsized?
    let downsizedStill: DownsizedStill?
    let downsizedLarge: DownsizedLarge?
    let downsizedMedium: DownsizedMedium?
    let downsizedSmall: DownsizedSmall?
    let original: Original?
    let originalStill: OriginalStill?
    let looping: Looping?
    let preview: Preview?
    let previewGif: PreviewGif?
    
}
