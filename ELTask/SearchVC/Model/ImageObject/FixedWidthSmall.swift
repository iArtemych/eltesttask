//
//  fixedWidthSmall.swift
//  ELTask
//
//  Created by Artem Chursin on 18.02.2020.
//  Copyright © 2020 Artem Chursin. All rights reserved.
//

import Foundation

struct FixedWidthSmall: Decodable {
    
    let url: String?
    let width: String?
    let height: String?
    let size: String?
    let mp4: String?
    let mp4Size: String?
    let webp: String?
    let webpSize: String?
}
