//
//  DownsizedLarge.swift
//  ELTask
//
//  Created by Artem Chursin on 18.02.2020.
//  Copyright © 2020 Artem Chursin. All rights reserved.
//

import Foundation

struct DownsizedLarge: Decodable {
    
    let url: String?
    let width: String?
    let height: String?
    let size: String?
}
