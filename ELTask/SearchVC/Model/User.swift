//
//  User.swift
//  ELTask
//
//  Created by Artem Chursin on 18.02.2020.
//  Copyright © 2020 Artem Chursin. All rights reserved.
//

import Foundation

struct User: Decodable {
    
    let avatarUrl: String?
    let bannerUrl: String?
    let profileUrl: String?
    let username: String?
    let displayName: String?
    
}
