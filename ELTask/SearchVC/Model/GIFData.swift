//
//  GIFData.swift
//  ELTask
//
//  Created by Artem Chursin on 18.02.2020.
//  Copyright © 2020 Artem Chursin. All rights reserved.
//

import Foundation

struct GIFData: Decodable {
    
    let data: [GIFObject]
    let pagination: PaginationObject
    let meta: MetaObject
}
