//
//  PaginationObject.swift
//  ELTask
//
//  Created by Artem Chursin on 18.02.2020.
//  Copyright © 2020 Artem Chursin. All rights reserved.
//

import Foundation

struct PaginationObject: Decodable {
    
    let offset: Int?
    let totalCount: Int?
    let count: Int?
}
