//
//  GIFObject.swift
//  ELTask
//
//  Created by Artem Chursin on 18.02.2020.
//  Copyright © 2020 Artem Chursin. All rights reserved.
//

import Foundation

struct GIFObject: Decodable {
    
    let type: String?
    let id: String?
    let slug: String?
    let url: String?
    let bitlyUrl: String?
    let embedUrl: String?
    let username: String?
    let source: String?
    let rating: String?
    let contentUrl: String?
    let user: User?
    let sourceTld: String?
    let sourcePostUrl: String?
    let updateDatetime: String?
    let createDatetime: String?
    let importDatetime: String?
    let trendingDatetime: String?
    let images: ImageObject?
    let title: String?
    
}
