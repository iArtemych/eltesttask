//
//  FullScreenGIFViewController.swift
//  ELTask
//
//  Created by Artem Chursin on 19.02.2020.
//  Copyright © 2020 Artem Chursin. All rights reserved.
//

import UIKit

class FullScreenGIFViewController: UIViewController {
    
    //MARK: - Variables
    var imageURL: GIFObject?
    
    //MARK: - Outlets
    @IBOutlet weak var gifImage: UIImageView!
    
    //MARK: - LifeStyle ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = imageURL?.title
        
        setupImageView()
    }
    
    //MARK: - Private methods
    private func setupImageView() {
        
        guard let url = imageURL?.images?.fixedHeight?.url else {
            return
        }
        gifImage.image = UIImage.gif(url: url)
    }
}
