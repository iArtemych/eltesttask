//
//  NetworkManager.swift
//  ELTask
//
//  Created by Artem Chursin on 18.02.2020.
//  Copyright © 2020 Artem Chursin. All rights reserved.
//

import Foundation
import UIKit

class NetworkManager {
    
    static func fetchData(url: String, completion: @escaping (_ searchedGIFS: GIFData) -> ()) {
        
        guard let url = URL(string: url) else {
            return
        }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let gifs = try decoder.decode(GIFData.self, from: data)
                completion(gifs)
            } catch let error {
                print("Error with serialization json", error)
            }
        }.resume()
    }
}

